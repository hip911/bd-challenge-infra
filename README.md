# Birthday Challenge

### Task
Create a no-downtime production deployment for a simple web app.

### Solution
#### Webapp

* find the webapp here: https://gitlab.com/hip911/bd-challenge

* PHP, Lumen microframework. all custom code resides in routes/web.php

* above repo is self-containing, the app itself plus code for the hosted GitlabCI execution

* has stages for installing dependencies, unittesting, end-to-end testing with docker-compose and pytest

* also builds an idempotent migration container, which can be executed as a one-off task in ECS


#### Infra

* the task felt it should be from the ground up, so instead of 'assuming' Kubernetes is there I put together an ECS setup which can be launched from the ground up

* used a decent, previously existing ECS setup from https://github.com/arminc/terraform-ecs

* extended with an RDS instance (only for simplicity)

* extended with ECS-Service-Autoscaling with needs Cloudwatch

* changed the EC2 instances to Spot Instances, which are also auto-scaling and also needs Cloudwatch

* provided customized deploy and migrate scripts

* ECS deployment will to rolling-update 

#### What is missing

* in CI after building the docker continer, one could deploy automatically

* in CI currently there is no interaction with the DB/migrations, this is not ideal, not failing fast enough 

* aspects of handling secrets were ignored 

* dynamic population of ENV variables from current terraform state to be applied on the deployment scripts
