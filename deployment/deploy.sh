#!/bin/sh -e

NAME='bd-challenge'
IMAGE='hip911/bd-challenge'
DB_HOST='terraform-20180531114113208900000001.crjsdc5yghal.eu-west-1.rds.amazonaws.com'
DB_USERNAME='testuser'
DB_PASSWORD='testpass'
PORT='80'

# register task-definition
sed <td-bd-challenge.template -e "s,@NAME@,$NAME,g;s,@IMAGE@,$IMAGE,g;s/@DB_HOST@/$DB_HOST/g;s/@DB_USERNAME@/$DB_USERNAME/g;s/@DB_PASSWORD@/$DB_PASSWORD/g;s,@PORT@,$PORT,g">TASKDEF.json
aws ecs register-task-definition --cli-input-json file://TASKDEF.json > REGISTERED_TASKDEF.json --region=eu-west-1
TASKDEFINITION_ARN=$( < REGISTERED_TASKDEF.json jq .taskDefinition.taskDefinitionArn )

# update service
sed "s,\"@@TASKDEFINITION_ARN@@\",$TASKDEFINITION_ARN," <update-service.json >SERVICEDEF.json
aws ecs update-service --cli-input-json file://SERVICEDEF.json > SERVICE.json --region=eu-west-1
