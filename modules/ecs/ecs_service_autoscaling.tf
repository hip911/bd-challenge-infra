resource "aws_appautoscaling_target" "ecs_target" {
  max_capacity       = "${var.app_max_size}"
  min_capacity       = "${var.app_min_size}"
  resource_id        = "service/${var.cluster}/bd-challenge"
  role_arn           = "${aws_iam_role.ecs_appautoscaling_role.arn}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "ecs_scale_up_policy" {
  name                    = "scale-up"
  policy_type             = "StepScaling"
  resource_id             = "service/${var.cluster}/bd-challenge"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "PercentChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Maximum"

    step_adjustment {
      metric_interval_upper_bound = 5
      metric_interval_lower_bound = 0
      scaling_adjustment = 5
    }

    step_adjustment {
      metric_interval_upper_bound = 10
      metric_interval_lower_bound = 5
      scaling_adjustment = 15
    }

    step_adjustment {
      metric_interval_lower_bound = 10
      scaling_adjustment = 30
    }
  }

  depends_on = ["aws_appautoscaling_target.ecs_target"]
}

resource "aws_cloudwatch_metric_alarm" "testalarm-up" {
  alarm_name          = "terraform-testalarm-up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "80"

  dimensions {
    ServiceName = "bd-challenge"
  }

  alarm_description = "This metric monitors ecs service cpu utilization"
  alarm_actions     = ["${aws_appautoscaling_policy.ecs_scale_up_policy.arn}"]
}

resource "aws_appautoscaling_policy" "ecs_scale_down_policy" {
  name                    = "scale-down"
  policy_type             = "StepScaling"
  resource_id             = "service/${var.cluster}/bd-challenge"
  scalable_dimension      = "ecs:service:DesiredCount"
  service_namespace       = "ecs"

  step_scaling_policy_configuration {
    adjustment_type         = "PercentChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Maximum"

    step_adjustment {
      metric_interval_upper_bound = 0
      metric_interval_lower_bound = -45
      scaling_adjustment          = -5
    }

  }

  depends_on = ["aws_appautoscaling_target.ecs_target"]
}

resource "aws_cloudwatch_metric_alarm" "testalarm-down" {
  alarm_name          = "terraform-testalarm-down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "20"

  dimensions {
    ServiceName = "bd-challenge"
  }

  alarm_description = "This metric monitors ecs service cpu utilization"
  alarm_actions     = ["${aws_appautoscaling_policy.ecs_scale_down_policy.arn}"]
}


resource "aws_iam_role" "ecs_appautoscaling_role" {
  name = "${var.environment}_ecs_appautoscaling_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["application-autoscaling.amazonaws.com"]
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs_autoscale_role" {
  role       = "${aws_iam_role.ecs_appautoscaling_role.id}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceAutoscaleRole"
}