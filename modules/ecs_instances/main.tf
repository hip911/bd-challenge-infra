# You can have multiple ECS clusters in the same account with different resources.
# Therefore all resources created here have the name containing the name of the:
# environment, cluster name en the instance_group name.
# That is also the reason why ecs_instances is a seperate module and not everything is created here.

resource "aws_security_group" "instance" {
  name        = "${var.environment}_${var.cluster}_${var.instance_group}"
  description = "Used in ${var.environment}"
  vpc_id      = "${var.vpc_id}"

  tags {
    Environment   = "${var.environment}"
    Cluster       = "${var.cluster}"
    InstanceGroup = "${var.instance_group}"
  }
}

# We separate the rules from the aws_security_group because then we can manipulate the 
# aws_security_group outside of this module
resource "aws_security_group_rule" "outbound_internet_access" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.instance.id}"
}

resource "aws_spot_fleet_request" "main" {
  iam_fleet_role                      = "${var.iam_fleet_role}"
  spot_price                          = "${var.spot_prices[0]}"
  allocation_strategy                 = "${var.strategy}"
  target_capacity                     = "${var.instance_count}"
  terminate_instances_with_expiration = true
  valid_until                         = "${var.valid_until}"
  target_group_arns                   = ["${var.target_group_arn}"]

  launch_specification {
    ami                    = "${var.aws_ami}"
    instance_type          = "${var.instance_type}"
    spot_price             = "${var.spot_prices[0]}"
    subnet_id              = "${var.private_subnet_ids[0]}"
    vpc_security_group_ids = ["${aws_security_group.instance.id}"]
    iam_instance_profile_arn   = "${var.iam_instance_profile_arn}"
    key_name               = "${var.key_name}"

    root_block_device = {
      volume_type = "gp2"
      volume_size = "${var.volume_size}"
    }

    user_data = "${data.template_file.user_data.rendered}"

    tags {
      Name = "${var.environment}_ecs_${var.cluster}_${var.instance_group}"
      Environment = "${var.environment}"
      Cluster = "${var.cluster}"
      InstanceGroup = "${var.instance_group}"
    }
  }

  launch_specification {
    ami                    = "${var.aws_ami}"
    instance_type          = "${var.instance_type}"
    spot_price             = "${var.spot_prices[1]}"
    subnet_id              = "${var.private_subnet_ids[0]}"
    vpc_security_group_ids = ["${aws_security_group.instance.id}"]
    iam_instance_profile_arn = "${var.iam_instance_profile_arn}"
    key_name               = "${var.key_name}"

    root_block_device = {
      volume_type = "gp2"
      volume_size = "${var.volume_size}"
    }

    user_data = "${data.template_file.user_data.rendered}"

    tags {
      Name = "${var.environment}_ecs_${var.cluster}_${var.instance_group}"
      Environment = "${var.environment}"
      Cluster = "${var.cluster}"
      InstanceGroup = "${var.instance_group}"
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}


data "template_file" "user_data" {
  template = "${file("${path.module}/templates/user_data.sh")}"

  vars {
    ecs_config        = "${var.ecs_config}"
    ecs_logging       = "${var.ecs_logging}"
    cluster_name      = "${var.cluster}"
    env_name          = "${var.environment}"
    custom_userdata   = "${var.custom_userdata}"
    cloudwatch_prefix = "${var.cloudwatch_prefix}"
  }
}
