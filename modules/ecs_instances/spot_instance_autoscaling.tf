resource "aws_appautoscaling_target" "workers_target" {
  min_capacity       = "${var.workers_min_instances}"
  max_capacity       = "${var.workers_max_instances}"
  resource_id        = "spot-fleet-request/${aws_spot_fleet_request.main.id}"
  role_arn           = "${aws_iam_role.workers_scaling.arn}"
  scalable_dimension = "ec2:spot-fleet-request:TargetCapacity"
  service_namespace  = "ec2"
}

resource "aws_appautoscaling_policy" "workers_scale_up" {
  name                    = "workers-scaling-up"
  resource_id             = "spot-fleet-request/${aws_spot_fleet_request.main.id}"
  scalable_dimension      = "ec2:spot-fleet-request:TargetCapacity"
  service_namespace       = "ec2"

  step_scaling_policy_configuration {
    adjustment_type = "PercentChangeInCapacity"
    cooldown = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 5
      metric_interval_lower_bound = 0
      scaling_adjustment = 5
    }

    step_adjustment {
      metric_interval_upper_bound = 10
      metric_interval_lower_bound = 5
      scaling_adjustment = 15
    }

    step_adjustment {
      metric_interval_lower_bound = 10
      scaling_adjustment = 30
    }
  }

  depends_on = ["aws_appautoscaling_target.workers_target"]
}

resource "aws_appautoscaling_policy" "workers_scale_down" {
  name                    = "workers-scaling"
  resource_id             = "spot-fleet-request/${aws_spot_fleet_request.main.id}"
  scalable_dimension      = "ec2:spot-fleet-request:TargetCapacity"
  service_namespace       = "ec2"

  step_scaling_policy_configuration {
    adjustment_type = "PercentChangeInCapacity"
    cooldown = 60
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_upper_bound = 0
      metric_interval_lower_bound = -45
      scaling_adjustment          = -5
    }
  }

  depends_on = ["aws_appautoscaling_target.workers_target"]
}

resource "aws_cloudwatch_metric_alarm" "add_capacity" {
  alarm_name          = "spot-${aws_spot_fleet_request.main.id}_upscaling"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 3
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2Spot"
  period              = 60
  statistic           = "Average"
  threshold           = "20"
  treat_missing_data  = "missing"

  dimensions {
    FleetRequestId = "${aws_spot_fleet_request.main.id}"
  }

  alarm_description = "Autoscaling CPUUtilization too high"
  alarm_actions     = ["${aws_appautoscaling_policy.workers_scale_up.arn}"]
}

resource "aws_cloudwatch_metric_alarm" "remove_capacity" {
  alarm_name          = "spot-${aws_spot_fleet_request.main.id}_downscaling"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 3
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2Spot"
  period              = 60
  statistic           = "Average"
  threshold           = "3"
  treat_missing_data  = "missing"

  dimensions {
    FleetRequestId = "${aws_spot_fleet_request.main.id}"
  }

  alarm_description = "Autoscaling CPUUtilization too high"
  alarm_actions     = ["${aws_appautoscaling_policy.workers_scale_down.arn}"]
}

resource "aws_iam_role" "workers_scaling" {
  name = "iam_for_workers_scaling"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["application-autoscaling.amazonaws.com"]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "aws_iam_policy_document" "workers_scaling" {
  "statement" = {
    "effect" = "Allow"

    "actions" = [
      "application-autoscaling:RegisterScalableTarget",
      "cloudwatch:DescribeAlarms",
      "ec2:DescribeSpotFleetRequests",
      "ec2:ModifySpotFleetRequest",
    ]

    "resources" = [
      "*",
    ]
  }
}

resource "aws_iam_role_policy" "concourse_workers_scaling" {
  name   = "iam_for_concourse_workers_scaling"
  role   = "${aws_iam_role.workers_scaling.id}"
  policy = "${data.aws_iam_policy_document.workers_scaling.json}"
}