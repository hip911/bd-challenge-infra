variable "rds_username" {
  default     = ""
  description = "RDS username"
}

variable "rds_password" {
  default     = ""
  description = "RDS password"
}

variable "private_subnet_ids" {
  type        = "list"
  description = "The list of private subnets to place the instances in"
}

variable "ecs_instance_security_group_id" {
  type        = "string"
  description = ""
}