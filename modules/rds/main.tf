resource "aws_db_instance" "rds-mydb" {
  allocated_storage    = 10
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "mydb"
  username             = "${var.rds_username}"
  password             = "${var.rds_password}"
  parameter_group_name = "default.mysql5.7"
  vpc_security_group_ids = ["${var.ecs_instance_security_group_id}"]
  db_subnet_group_name = "${aws_db_subnet_group.default.name}"
  skip_final_snapshot = true
}


resource "aws_db_subnet_group" "default" {
  name        = "main_subnet_group"
  description = "Our main group of subnets"
  subnet_ids  = ["${var.private_subnet_ids}"]
}