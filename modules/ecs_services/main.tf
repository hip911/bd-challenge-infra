data "template_file" "bd-challenge" {
  template = "${file("${path.module}/task-definitions/bd-challenge.json.tmpl")}"

  vars {
    rds_endpoint = "${var.rds_endpoint}"
    rds_username = "${var.rds_username}"
    rds_password = "${var.rds_password}"
  }
}

data "template_file" "bd-challenge-migration" {
  template = "${file("${path.module}/task-definitions/bd-challenge-migration.json.tmpl")}"

  vars {
    rds_endpoint = "${var.rds_endpoint}"
    rds_username = "${var.rds_username}"
    rds_password = "${var.rds_password}"
  }
}

resource "aws_ecs_service" "bd-challenge-service" {
  name = "bd-challenge"
  cluster = "${var.ecs_cluster_id}"
  task_definition = "${aws_ecs_task_definition.bd-challenge.arn}"
  iam_role = "${var.ecs_service_role}"
  desired_count = 2

  load_balancer {
    target_group_arn = "${var.target_group_arn}"
    container_name = "bd-challenge"
    container_port = 80
  }
}

resource "aws_ecs_task_definition" "bd-challenge" {
  family = "bd-challenge"
  container_definitions = "${data.template_file.bd-challenge.rendered}"
}

resource "aws_ecs_task_definition" "bd-challenge-migration" {
  family = "bd-challenge-migration"
  container_definitions = "${data.template_file.bd-challenge-migration.rendered}"
}